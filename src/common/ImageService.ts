import { ImageURISource } from 'react-native';

export class ImageService {
  static readonly addImage: ImageURISource = require('../../assets/images/addImage.png');
  static readonly delete: ImageURISource = require('../../assets/images/delete.png');
}