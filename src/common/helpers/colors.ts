export enum Color {
  white = '#FFFFFF',
  whiteTransparent = 'rgba(255,255,255,0.5)',
  black = '#000000',
  red = '#990000',
  green = '#009900',
  blue = '#000099',
  darkBlue = '#18027E',
  blueBell = '#A48FFF',
  gray = 'gray',
  transparent = 'transparent',
}