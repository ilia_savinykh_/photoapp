import { ViewStyle, TextStyle, ImageStyle } from 'react-native';
import { platform } from './platform';

export const style = {
  view: (s: ViewStyle): ViewStyle => s,
  text: (s: TextStyle): TextStyle => s,
  image: (s: ImageStyle): ImageStyle => s,
}

export const fonts = {
  medium: "Roboto-Medium",
  regular: "Roboto-Regular",
};