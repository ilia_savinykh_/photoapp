import { Dimensions } from 'react-native';

const data = Dimensions.get('window');

export const windowWidth: number = data.width;
export const windowHeight: number = data.height;