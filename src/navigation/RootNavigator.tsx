import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { AddPhotoMainScreen } from '../screens/addPhoto/containers/AddPhotoMainScreen';
import { pages } from './navigation';
import { stackOptions } from './stackOptions';
import { GalleryMainScreen } from '../screens/gallery/containers/GalleryMainScreen';
import { PhotoMainScreen } from '../screens/Photo/components/PhotoMainScreen';

const Stack = createStackNavigator();

export const RootNavigator = (): JSX.Element => {
  return (
    <Stack.Navigator initialRouteName={pages.addPhoto}>
      <Stack.Screen 
        name={pages.addPhoto}
        component={AddPhotoMainScreen}
        options={stackOptions.noHeader}
      />
      <Stack.Screen
        name={pages.gallery}
        component={GalleryMainScreen}
        options={stackOptions.noHeader}
      />
      <Stack.Screen
        name={pages.showPhoto}
        component={PhotoMainScreen}
        options={stackOptions.noHeader}
      />
    </Stack.Navigator>
  );
}