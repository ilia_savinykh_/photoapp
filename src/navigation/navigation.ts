import { ParamListBase, RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { IPhoto } from "../core/store/rootState";

export interface StackNavigationsProps<ParamsList extends ParamListBase, RouteName extends keyof ParamsList = string>{
  navigation: StackNavigationProp<ParamsList, RouteName>;
  route: RouteProp<ParamsList, RouteName>;
}

export enum pages {
  addPhoto = 'addPhoto',
  gallery = 'gallery',
  showPhoto = 'showPhoto',
}

export type Routes = {
  [pages.addPhoto]: undefined;
  [pages.gallery]: undefined;
  [pages.showPhoto]: IPhoto;
}