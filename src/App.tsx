import { NavigationContainer } from '@react-navigation/native';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import { Persistor } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { configureStore } from './core/store';
import { RootNavigator } from './navigation/RootNavigator';

interface IProps {}

export class App extends Component {
  private readonly store: Store;
  private readonly persisted: Persistor;

  constructor(props: IProps) {
    super(props);
    const { store, persisted } = configureStore();
    this.store = store;
    this.persisted = persisted;
  }

  render(): JSX.Element {
    return (
      <PersistGate persistor={this.persisted}>
        <Provider store={this.store}>
          <NavigationContainer>
            <RootNavigator />
          </NavigationContainer>
        </Provider>
      </PersistGate>
    );
  }
}