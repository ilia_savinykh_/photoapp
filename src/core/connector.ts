import { connect } from "react-redux";
import { Dispatch } from "redux";
import { IReduxProps } from "../common/containers/BaseComponent";
import { IRootState } from "./store/rootState";

export function connector<StateProps, DispatchProps>(
  mapState: ((state: IRootState, ownProps: any) => StateProps) | null,
  mapDispatch?: (dispatch: Dispatch, ownProps: any) => DispatchProps
): any {
  return connect(mapState, mapDispatch as any, merge) as any;
}

function merge<IStateProps, IDispatchProps>(
  stateProps: IStateProps,
  dispatchProps: IDispatchProps,
  ownProps: any,
): IReduxProps<IStateProps, IDispatchProps> {
  return Object.assign({}, ownProps, {
    stateProps,
    dispatchProps,
  });
}