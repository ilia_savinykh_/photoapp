export interface IRootState {
  gallery: IPhoto[] | null;
}

export interface IPhoto {
  id: string,
  uri: string;
  createTime: Date;
}

export type ISendPhotoRequest = IPhoto[];
export type ISendPhotoResponse = IPhoto[];

export const RootInitialState: IRootState = {
  gallery: null,
}