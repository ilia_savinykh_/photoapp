import { Failure, Success } from "typescript-fsa";
import { ReducerBuilder, reducerWithInitialState } from "typescript-fsa-reducers";
import { sendPhotoAsyncAction } from "./rootActions";
import { IPhoto, IRootState, ISendPhotoRequest, ISendPhotoResponse, RootInitialState } from "./rootState";

function sendPhotoDone(state: IRootState, payload: Success<ISendPhotoRequest, ISendPhotoResponse>): IRootState {
  const gallery: IPhoto[] = state.gallery !== null ? state.gallery : [];
  for (let key in payload.result) {
    gallery?.push(payload.result[key])
  }
  return {
    ...state,
    gallery, 
  };
}

export const rootReducer: ReducerBuilder<IRootState> = reducerWithInitialState(RootInitialState)
  .case(sendPhotoAsyncAction.async.done, sendPhotoDone)