import { REHYDRATE } from "redux-persist";
import { sendPhoto } from "../../screens/addPhoto/api/request";
import { actionCreator, asyncActionCreate } from "../actionCreator";
import { IRootState, ISendPhotoRequest, ISendPhotoResponse } from "./rootState";

export const sendPhotoAsyncAction = asyncActionCreate<ISendPhotoRequest, ISendPhotoResponse, Error>(
  'ROOT/SEND_PHOTO',
  async (params): Promise<ISendPhotoResponse> => {
    return sendPhoto(params);
  }
)

export const rehydrateAction = actionCreator<IRootState>(REHYDRATE);