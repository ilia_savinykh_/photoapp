import { actionCreatorFactory } from "typescript-fsa";
import { asyncFactory } from 'typescript-fsa-redux-thunk';
import { IRootState } from "./store/rootState";

export const actionCreator = actionCreatorFactory();
export const asyncActionCreate = asyncFactory<IRootState>(actionCreator);