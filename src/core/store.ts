import AsyncStorage from "@react-native-community/async-storage";
import { applyMiddleware, createStore, Middleware, Store } from "redux";
import logger from "redux-logger";
import { Persistor, persistReducer, persistStore } from "redux-persist";
import thunk from "redux-thunk";
import { rootReducer } from "./store/rootReducer";
import { IRootState } from "./store/rootState";

export interface IConfigureStore {
  store: Store,
  persisted: Persistor,
}

let middleware: Middleware[] = [thunk];

if (__DEV__) {
  middleware = [...middleware, logger];
} else {
  middleware = [...middleware];
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['gallery']
}

const persistReduce = persistReducer(persistConfig, rootReducer);

export function configureStore(): IConfigureStore {
  const store: Store = createStore(persistReduce, applyMiddleware(...middleware));
  const persisted: Persistor = persistStore(store);
  return { store, persisted };
} 