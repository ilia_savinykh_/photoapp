import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, Alert } from 'react-native';
import Swiper from 'react-native-swiper';
import { ThunkDispatch } from 'redux-thunk';
import { BaseComponent } from '../../../common/containers/BaseComponent';
import { Color, style, styleSheetCreate, windowHeight, windowWidth } from '../../../common/helpers';
import { ImageService } from '../../../common/ImageService';
import { connector } from '../../../core/connector';
import { sendPhotoAsyncAction } from '../../../core/store/rootActions';
import { IPhoto, IRootState, ISendPhotoRequest } from '../../../core/store/rootState';
import { pages, Routes, StackNavigationsProps } from '../../../navigation/navigation';

import ImagePicker from 'react-native-image-picker';

interface IStateProps {
  gallery: IPhoto[] | null;
}

interface IDispatchProps {
  sendPhoto(params: ISendPhotoRequest): void;
}

interface IProps extends StackNavigationsProps<Routes, pages.addPhoto>{}

interface IState {
  photos: IPhoto[] | null,
}

@connector(
  (state: IRootState): IStateProps => ({
    gallery: state.gallery,
  }),
  (dispatch: ThunkDispatch<IRootState, void, {type: string, payload: any}>): IDispatchProps => ({
    sendPhoto(params: ISendPhotoRequest): void {
      dispatch(sendPhotoAsyncAction(params))
    }
  })
)

export class AddPhotoMainScreen extends BaseComponent<IStateProps, IDispatchProps, IProps, IState> {

  state: IState = {
    photos: null,
  }

  constructor(props: IProps) {
    super(props);
  }

  deletePhoto = (index: number): void => {
    const photos: IPhoto[] | null = this.state.photos;
    if (photos) {
      if (photos.length === 1) {
        this.setState({ photos: null });
        return;
      }
      photos.splice(index, 1);
      this.setState({ photos });
    }
  }

  selectPhoto = (): void => {
    if (this.state.photos?.length === 5) return;

    ImagePicker.showImagePicker({
      title: `Добавление фото (${this.state.photos ? this.state.photos.length + 1 : 1}/5)`,
      takePhotoButtonTitle: 'Сделать фото',
      chooseFromLibraryButtonTitle: 'Выбрать фото из галереи',
      quality: 1,
    }, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {

        const data = 'data:image/jpeg;base64,' + response.data;
      
        const photos: IPhoto[] = this.state.photos ? this.state.photos : [];

        photos.push({
          id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
          uri: data,
          createTime: new Date(),
        });

        this.setState({
          photos,
        });

      }
    });
  }

  addPhoto = (): void => {
    if (this.state.photos) {
      if (this.state.photos.length !== 5) {
        Alert.alert('Ошибка', `Выберите 5 фотографий, чтобы можно было их сохранить в галерее. Вы выбрали ${this.state.photos.length} из 5`);
        return;
      } else {
        this.dispatchProps.sendPhoto(this.state.photos);
        this.setState({ photos: null });
      }
    } else {
      Alert.alert('Ошибка', `Выберите 5 фотографий, чтобы можно было их сохранить в галерее. Вы выбрали 0 из 5`);
    }
  }

  renderPhotoList = (): JSX.Element[] | null => {
    if (!this.state.photos) return null;
    const list: JSX.Element[] = this.state.photos.map((photo: IPhoto, index: number) => {
      return (
        <View style={styles.flex} key={photo.id}>
          <TouchableOpacity
            onPress={this.deletePhoto.bind(this, index)}
            style={styles.deleteContainer}
          >
            <Image source={ImageService.delete} style={styles.deleteImage} />
          </TouchableOpacity>
          <Image
            resizeMode={'contain'}
            source={{uri: photo.uri}}
            style={styles.sliderImage}
          />
        </View>
      );
    });
    return list;
  }

  renderPhoto = (): JSX.Element => {
    if (this.state.photos) {
      return (
        <View style={styles.photoContainer}>
            <Swiper
              loop={false}
              showsButtons={true}
              showsPagination={false}
            >
              {this.renderPhotoList()}
            </Swiper>
          <TouchableOpacity
            onPress={this.selectPhoto}
            style={styles.selectPhotoContainer}
          >
            <Image
              source={ImageService.addImage}
              style={styles.selectPhotoImage}
            />
            <Text style={styles.selectPhotoText}>
              {`${this.state.photos ? this.state.photos.length : 1}/5`}
            </Text>
          </TouchableOpacity>
        </View>
      ); 
    } else {
      return (
        <TouchableOpacity
          onPress={this.selectPhoto}
          style={styles.photoContainer}
        >
          <Text style={styles.selectEmptyPhotoText}>
            {'Нажмите, чтобы добавить фото'}
          </Text>
            <Image
              source={ImageService.addImage}
              style={styles.selectPhotoImage}
            />
        </TouchableOpacity>
      );
    }
  }

  navigateToGallery = (): void => {
    this.props.navigation.navigate(pages.gallery);
  }

  render() {
    const havePhotos: boolean = this.stateProps.gallery !== null;
    const disabled: boolean = this.state.photos ? (this.state.photos.length === 5 ? false : true ) : true; 
    return (
      <SafeAreaView style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerText}>
            {'Добавить фото'}
          </Text>
          <TouchableOpacity
            onPress={havePhotos ? this.navigateToGallery : undefined}
            style={styles.galleryButton}
          >
            <Text style={styles.galleryButtonText}>
              {'Галерея'}
            </Text>
          </TouchableOpacity>
        </View>

        {this.renderPhoto()}

        <TouchableOpacity
          disabled={disabled}
          onPress={this.addPhoto}
          style={styles.addPhotoButton}
        >
          <Text style={styles.addPhotoButtonText}>
            {'Добавить'}
          </Text>
        </TouchableOpacity>

      </SafeAreaView>
    );
  }
}

const styles = styleSheetCreate({
  container: style.view({
    flex: 1,
    backgroundColor: Color.blueBell,
    alignItems: 'center',
    justifyContent: 'center'
  }),
  header: style.view({
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: windowWidth * .03
  }),
  headerText: style.text({
    flex: 1,
    textAlign: 'center',
    fontSize: windowWidth * .05,
    fontWeight: '600'
  }),
  galleryButton: style.view({
    marginRight: windowWidth * .05,
    backgroundColor: Color.darkBlue,
    borderRadius: windowWidth * .02
  }),
  galleryButtonText: style.text({
    color: Color.white,
    fontWeight: '600',
    paddingHorizontal: windowWidth * .05,
    paddingVertical: windowWidth * .02
  }),
  addPhotoButton: style.view({
    width: windowWidth * .9,
    backgroundColor: Color.darkBlue,
    borderRadius: windowWidth * .02,
    marginVertical: windowWidth * .03,
    alignItems: 'center'
  }),
  addPhotoButtonText: style.text({
    color: Color.white,
    fontWeight: '600',
    paddingHorizontal: windowWidth * .05,
    paddingVertical: windowWidth * .03
  }),
  flex: style.view({ flex: 1 }),
  deleteContainer: style.view({
    position: 'absolute',
    top: windowHeight * .03,
    right: windowHeight * .03,
    zIndex: 2
  }),
  deleteImage: style.image({
    width: windowHeight * .05,
    height: windowHeight * .05
  }),
  sliderImage: style.image({
    width: '100%',
    height: '100%'
  }),
  photoContainer: style.view({
    flex: 1,
    backgroundColor: Color.whiteTransparent,
    width: windowWidth * .9,
    alignItems: 'center',
    justifyContent: 'center'
  }),
  selectPhotoContainer: style.view({
    position: 'absolute',
    bottom: windowHeight * .07,
    alignItems: 'center'
  }),
  selectPhotoImage: style.image({
    width: windowHeight * .07,
    height: windowHeight * .07
  }),
  selectPhotoText: style.text({
    fontSize: windowHeight * .025,
    color: Color.darkBlue,
    backgroundColor: Color.white
  }),
  selectEmptyPhotoText: style.text({
    fontSize: windowHeight * .05,
    textAlign: 'center',
    color: Color.gray,
    marginBottom: windowHeight * .03
  }),
});