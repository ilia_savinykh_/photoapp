import moment from 'moment';
import React from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, FlatList, Image } from 'react-native';
import { BaseComponent } from '../../../common/containers/BaseComponent';
import { Color, style, styleSheetCreate, windowHeight, windowWidth } from '../../../common/helpers';
import { connector } from '../../../core/connector';
import { IPhoto, IRootState } from '../../../core/store/rootState';
import { pages, Routes, StackNavigationsProps } from '../../../navigation/navigation';

interface IStateProps {
  gallery: IPhoto[] | null;
}

interface IProps extends StackNavigationsProps<Routes, pages.gallery>{}

@connector(
  (state: IRootState): IStateProps => ({
    gallery: state.gallery,
  })
)

export class GalleryMainScreen extends BaseComponent<IStateProps, {}, IProps, {}>  {

  constructor(props: IProps) {
    super(props);
  }

  _back = () => {
    this.props.navigation.goBack();
  }

  navigateToPhoto = (photo: IPhoto) => {
    this.props.navigation.navigate(pages.showPhoto, photo);
  }

  renderPhoto = ({item}: {item: IPhoto, index: number}): JSX.Element => {
    return (
      <TouchableOpacity
        onPress={this.navigateToPhoto.bind(this, item)}
        style={styles.imageContainer}
      >
        <View style={styles.flex}>
          <Image
            resizeMode={'contain'}
            source={{uri: item.uri}}
            style={styles.image}
          />
        </View>
        <Text style={{color: Color.gray, marginTop: windowHeight * .02}}>
          {'Дата создания:'}
          <Text style={{color: Color.darkBlue}}>{moment(item.createTime).format('DD/MM/YYYY HH:MM')}</Text>
        </Text>
      </TouchableOpacity>
    );
  }

  keyExtractor = (item: IPhoto, index: number): string => {
    return item.id;
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerText}>
            {'Галерея'}
          </Text>
          <TouchableOpacity
            onPress={this._back}
            style={styles.addPhotoButton}
          >
            <Text style={styles.addPhotoButtonText}>
              {'Добавить фото'}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.innerContainer}>
          {this.stateProps.gallery ? (
            <FlatList
              data={this.stateProps.gallery}
              renderItem={this.renderPhoto}
              keyExtractor={this.keyExtractor}
            />
          ) : (
            <Text>Null</Text>
          )}
        </View>

      </SafeAreaView>
    );
  }
}

const styles = styleSheetCreate({
  container: style.view({
    flex: 1,
    backgroundColor: Color.blueBell,
    alignItems: 'center',
    justifyContent: 'center'
  }),
  innerContainer: style.view({
    flex: 1,
    paddingBottom: windowHeight * .02,
  }),
  header: style.view({
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: windowWidth * .03
  }),
  headerText: style.text({
    flex: 1,
    textAlign: 'center',
    fontSize: windowWidth * .05,
    fontWeight: '600'
  }),
  addPhotoButton: style.view({
    marginRight: windowWidth * .05,
    backgroundColor: Color.darkBlue,
    borderRadius: windowWidth * .02
  }),
  addPhotoButtonText: style.text({
    color: Color.white,
    fontWeight: '600',
    paddingHorizontal: windowWidth * .05,
    paddingVertical: windowWidth * .02
  }),
  flex: style.view({ flex: 1 }),
  imageContainer: style.view({
    backgroundColor: Color.whiteTransparent,
    width: windowWidth * .9,
    minHeight: windowWidth,
    padding: windowHeight * .02,
    marginTop: windowHeight * .02,
  }),
  image: style.image({
    width: '100%',
    height: '100%'
  })
});