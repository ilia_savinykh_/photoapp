import React, { PureComponent } from 'react';
import { View, Text, Image, SafeAreaView, TouchableOpacity } from 'react-native';
import { pages, Routes, StackNavigationsProps } from '../../../navigation/navigation';
import moment from 'moment';
import { Color, style, styleSheetCreate, windowHeight, windowWidth } from '../../../common/helpers';

interface IProps extends StackNavigationsProps<Routes, pages.showPhoto>{}

export class PhotoMainScreen extends PureComponent<IProps> {

  constructor(props: IProps) {
    super(props);
  }

  _back = () => {
    this.props.navigation.goBack();
  }

  render() {
    const { uri, createTime } = this.props.route.params
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>
            {'Фотокарточка'}
          </Text>
          <TouchableOpacity
            onPress={this._back}
            style={styles.addPhotoButton}
          >
            <Text style={styles.addPhotoButtonText}>
              {'Галерея'}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.innerContainer}>
          <View style={styles.imageContainer}>
            <View style={styles.flex}>
              <Image
                resizeMode={'contain'}
                source={{uri: uri}}
                style={styles.image}
              />
            </View>
            <Text style={{color: Color.gray, marginTop: windowHeight * .02}}>
              {'Дата создания:'}
              <Text style={{color: Color.darkBlue}}>{moment(createTime).format('DD/MM/YYYY HH:MM')}</Text>
            </Text>
          </View>
        </View>

      </SafeAreaView>
    );
  }
}

const styles = styleSheetCreate({
  container: style.view({
    flex: 1,
    backgroundColor: Color.blueBell,
    alignItems: 'center',
    justifyContent: 'center'
  }),
  innerContainer: style.view({
    flex: 1,
    paddingBottom: windowHeight * .02,
  }),
  header: style.view({
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: windowWidth * .03
  }),
  headerText: style.text({
    flex: 1,
    textAlign: 'center',
    fontSize: windowWidth * .05,
    fontWeight: '600'
  }),
  addPhotoButton: style.view({
    marginRight: windowWidth * .05,
    backgroundColor: Color.darkBlue,
    borderRadius: windowWidth * .02
  }),
  addPhotoButtonText: style.text({
    color: Color.white,
    fontWeight: '600',
    paddingHorizontal: windowWidth * .05,
    paddingVertical: windowWidth * .02
  }),
  flex: style.view({ flex: 1 }),
  imageContainer: style.view({
    backgroundColor: Color.whiteTransparent,
    width: windowWidth * .9,
    minHeight: windowWidth,
    padding: windowHeight * .02,
    marginTop: windowHeight * .02,
  }),
  image: style.image({
    width: '100%',
    height: '100%'
  })
});